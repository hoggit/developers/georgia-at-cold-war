local base_char,keywords=128,{"and","break","do","else","elseif","end","false","for","function","if","in","local","nil","not","or","repeat","return","then","true","until","while",}; function prettify(code) return code:gsub("["..string.char(base_char).."-"..string.char(base_char+#keywords).."]", 
	function (c) return keywords[c:byte()-base_char]; end) end return assert(loadstring(prettify[===[� trigger.misc.getUserFlag(9999)==1 � HOGGIT � HOGGIT.script_base �
trigger.action.outText("DEBUG MODE ON",10)
HOGGIT.debug=�
HOGGIT.debug_text=�(e,t)
trigger.action.outText(e,t)
�
dofile(HOGGIT.script_base..[[\HOGGIT\hoggit\error_handling.lua]])
dofile(HOGGIT.script_base..[[\HOGGIT\hoggit\logging.lua]])
dofile(HOGGIT.script_base..[[\HOGGIT\hoggit\utils.lua]])
dofile(HOGGIT.script_base..[[\HOGGIT\hoggit\spawner.lua]])
dofile(HOGGIT.script_base..[[\HOGGIT\hoggit\communication.lua]])
dofile(HOGGIT.script_base..[[\HOGGIT\hoggit\group.lua]])
�
trigger.action.outText("DEBUG MODE OFF",10)
HOGGIT={}
HOGGIT.debug=�
HOGGIT.debug_text=�()�
�
� e=�(e)
log("Error in pcall: "..e)
log(debug.traceback())
� e
�
try=�(t,a)
� �()
� e,t=xpcall(t,e)
� � e �
� a(t)
�
� e
�
�
� HOGGIT.debug �
logFile=io.open(HOGGIT.log_base..[[\HOGGIT.log]],"w")
�
� log(e)
� e==� � e='nil'�
� HOGGIT.debug � logFile �
logFile:write("HOGGIT --- "..e.."\r\n")
logFile:flush()
�
env.info("HOGGIT --- "..e.."\r\n")
�
�
HOGGIT.randomInList=�(e)
� t=math.random(1,#e)
� e[t]
�
HOGGIT.filterTable=�(t,o)
� e={}
� a,t � pairs(t)�
� o(t)� e[a]=t �
�
� e
�
HOGGIT.listContains=�(t,e)
� a,t � ipairs(t)�
� t==e �
� �
�
�
� �
�
HOGGIT.getLatLongString=�(t,e)
� t,a=coord.LOtoLL(t)
� e==� � e=� �
� mist.tostringLL(t,a,3,e)
�
HOGGIT.getSmokeName=�(e)
� e==trigger.smokeColor.Green � �"Green"�
� e==trigger.smokeColor.Red � �"Red"�
� e==trigger.smokeColor.White � �"White"�
� e==trigger.smokeColor.Orange � �"Orange"�
� e==trigger.smokeColor.Blue � �"Blue"�
�
HOGGIT.GroupIsAlive=�(t)
� e=�
� type(t)=="string"�
e=Group.getByName(t)
�
e=t
�
� e � e:isExist()� e:getSize()>0 � � � � � � �
�
HOGGIT.zombies={}
HOGGIT.zombie_checks={}
HOGGIT.spawners={['neutral']={},['red']={},['blue']={}}
HOGGIT.Spawner=�(o)
� r=o
� e={}
� s={}
� a=�
� i=60
� h=�
� n=i
�{
Spawn=�(i)
� t=Group.getByName(mist.cloneGroup(o,�).name)
� e.func �
� � e.args � e.args={}�
mist.scheduleFunction(e.func,{t,unpack(e.args)},timer.getTime()+1)
�
s[t]=�
� a �
HOGGIT.setZombie(t,i,�)
HOGGIT.debug_text("Spawned new zombie "..t:getName(),3)
�
� t
�,
SpawnAtPoint=�(i,t)
� t={
groupName=o,
point=t,
action="clone"
}
� t=mist.teleportToPoint(t)
� t �
� t=t.name
� e.func �
� � e.args � e.args={}�
mist.scheduleFunction(e.func,{Group.getByName(t),unpack(e.args)},timer.getTime()+1)
�
� e=Group.getByName(t)
s[e]=�
� a � HOGGIT.setZombie(e,i,�)�
� e
�
HOGGIT.debug_text("Error spawning "..o,15)
�
�,
SpawnInZone=�(i,t)
� t=Group.getByName(mist.cloneInZone(o,t).name)
� e.func �
� � e.args � e.args={}�
mist.scheduleFunction(e.func,{t,unpack(e.args)},timer.getTime()+1)
�
s[t]=�
� a � HOGGIT.setZombie(t,i,�)�
� t
�,
OnSpawnGroup=�(o,a,t)
e.func=a
e.args=t
�,
SetGroupRespawnOptions=�(
o,
a,
t,
e)�
� e==� � t~=� �
e=a
�
o:SetZombieOptions({
['zombie']=�,
['respawn_delay']=a,
['partial_death_threshold_percent']=t,
['partial_death_respawn_delay']=e
})
�
�,
SetZombieOptions=�(t,e)
a=e['zombie']
� a �
i=e['respawn_delay']
h=e['partial_death_threshold_percent']
� e['partial_death_respawn_delay']�
n=e['partial_death_respawn_delay']
�
n=i
�
HOGGIT.debug_text(r.." is now a zombie!  Arggghhh!",10)
�
�,
GetRespawnDelay=�(t,e)
� e �
� n
�
� i
�
�,
GetPartialDeathThresholdPercent=�(e)
� h
�
}
�
HOGGIT.SetupDefaultSpawners=�()
� e,t � ipairs({'neutral','red','blue'})�
� a,e � pairs(coalition.getGroups(e-1))�
HOGGIT.spawners[t][Group.getName(e)]=HOGGIT.Spawner(Group.getName(e))
�
�
�
HOGGIT.setZombie=�(e,t,a)
� a �
HOGGIT.zombies[e:getName()]=t
�
HOGGIT.zombies[e:getName()]=�
�
�
HOGGIT._deathHandler=�(e)
� e.id==world.event.S_EVENT_CRASH � e.id==world.event.S_EVENT_DEAD �
HOGGIT.debug_text("SOMETHING DEAD YO",10)
� � e.initiator � � �
� � e.initiator.getGroup � � �
� � e.initiator:getGroup()� � �
� t=e.initiator:getGroup():getName()
� t �
HOGGIT.debug_text("FOUND GROUP",10)
� HOGGIT.zombies[t]�
HOGGIT.debug_text("CONFIRMED ZOMBIE",10)
� e=HOGGIT.zombies[t]
HOGGIT.debug_text("FOUND SPAWNER",10)
� HOGGIT.zombie_checks[e]�
HOGGIT.debug_text("FOUND SPAWNER CHECK",10)
� e=HOGGIT.zombie_checks[e]
mist.removeFunction(e)
HOGGIT.debug_text("Removing dead check id "..e.." for group: "..t,10)
�
� a=mist.scheduleFunction(�()
HOGGIT.debug_text("STARTING DEAD CHECK",10)
� a=�
� o=�
� � HOGGIT.GroupIsAlive(t)�
a=�
HOGGIT.debug_text("THEY DEAD, RESPAWNIN",10)
� e:GetPartialDeathThresholdPercent()�
HOGGIT.debug_text("CHECKING PERCENT",10)
� t=Group.getByName(t)
� i=t:getSize()
� t=t:getInitialSize()
� t=i/t*100
HOGGIT.debug_text(t.." percent alive.  Threshold is "..e:GetPartialDeathThresholdPercent(),10)
�(100-t)>=e:GetPartialDeathThresholdPercent()�
HOGGIT.debug_text("TRIGGERING PARTIAL RESPAWNING",10)
a=�
o=�
�
�
� a �
HOGGIT.debug_text("GROUP NEEDS RESPAWNING",10)
� a=e:GetRespawnDelay(o)
HOGGIT.debug_text("DELAY OF "..a,10)
HOGGIT.zombies[t]=�
mist.scheduleFunction(�()
HOGGIT.setZombie(e.Spawn(),e,�)
�,{},timer.getTime()+a)
�
HOGGIT.zombie_checks[e]=�
�,{},timer.getTime()+10)
HOGGIT.debug_text("Scheduled NEW dead check id "..a.." for group: "..t,10)
HOGGIT.zombie_checks[e]=a
�
�
�
�
mist.addEventHandler(HOGGIT._deathHandler)
HOGGIT.GroupCommandAdded={}
HOGGIT.GroupCommand=�(e,t,a,o)
� HOGGIT.GroupCommandAdded[tostring(e)]==� �
log("No commands from group "..e.." yet. Initializing menu state")
HOGGIT.GroupCommandAdded[tostring(e)]={}
�
� � HOGGIT.GroupCommandAdded[tostring(e)][t]�
log("Adding "..t.." to group: "..tostring(e))
callback=try(o,�(e)log("Error in group command"..e)�)
missionCommands.addCommandForGroup(e,t,a,callback)
HOGGIT.GroupCommandAdded[tostring(e)][t]=�
�
�
HOGGIT.GroupMenuAdded={}
HOGGIT.GroupMenu=�(e,t,a)
� HOGGIT.GroupMenuAdded[tostring(e)]==� �
log("No commands from groupId "..e.." yet. Initializing menu state")
HOGGIT.GroupMenuAdded[tostring(e)]={}
�
� � HOGGIT.GroupMenuAdded[tostring(e)][t]�
log("Adding "..t.." to groupId: "..tostring(e))
HOGGIT.GroupMenuAdded[tostring(e)][t]=missionCommands.addSubMenuForGroup(e,t,a)
�
� HOGGIT.GroupMenuAdded[tostring(e)][t]
�
HOGGIT.MessageToGroup=�(a,o,t,e)
� � t � t=10 �
� e==� � e=� �
trigger.action.outTextForGroup(a,o,t,e)
�
HOGGIT.MessageToAll=�(t,e)
� � e � e=10 �
trigger.action.outText(t,e)
�
HOGGIT.MessageToCoalition=�(t,e,a)
� � e � e=10 �
trigger.action.outTextForCoalition(a,t,e)
�
HOGGIT.groupCoords=�(e)
� e~=� �
� e:getUnits()[1]:getPosition().p
�
� �
�
HOGGIT.smokeAtGroup=�(t,e)
� t=HOGGIT.groupCoords(t)
� e==� � e=trigger.smokeColor.White �
trigger.action.smoke(t,e)
�
HOGGIT.CoordsForGroup=�(t,e)
� t==� � e==� � �""�
� t=t:getUnit(1)
� � t � �""�
� t=t:getTypeName()
� HOGGIT.CoordForPlaneType(t,e)
�
HOGGIT.CoordForPlaneType=�(o,e)
� t,e=coord.LOtoLL(e)
� i=�()
� mist.tostringLL(t,e,0,"")
�
� a=�()
� mist.tostringLL(t,e,3)
�
� n=�()
� mist.tostringMGRS(coord.LLtoMGRS(t,e),4)
�
� e=�()
� mist.tostringLL(e,t,2,"")
�
� e={
["Ka-50"]=a,
["M-2000C"]=a,
["A-10C"]=n,
["AJS37"]=e
}
� e=e[o]
� e � � e()� � i()�
�
]===], '@dist/hoggit_framework.lua'))()