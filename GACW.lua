--[[
    GACW.lua
    Georgia At Cold War PVE Alpha prototype.
    This is some quick and dirty code designed as an alpha to allow for fast iterations with the playerbase to see what concepts are fun and which ones aren't.
    By the completion of the Alpha into Beta we should formalize the key features of the code here and mayhaps even modularize and unit test it.  I think
    ideally we'd want to distribute this as a compiled lua jsut like the HOGGIT framework but we'll cross that bridge when we get to it.
]]

-- Setup JSON
local jsonlib = lfs.writedir() .. "Scripts\\GAW\\json.lua"
json = loadfile(jsonlib)()

HOGGIT.SetupDefaultSpawners()

-- Setup function to get a new marker ID.  This is used to setup the markers on the F10 map for the enemy bomber spawns and their target.
-- The F10 map marker code requires a unique ID for each marker, and it won't autogenerate one because it's a jerk.
local attack_marker_id = 9000
getMarkerId = function()
    attack_marker_id = attack_marker_id + 1
    return attack_marker_id
end

-- Keep up with the number of enemy bombers so we can tell when to spawn more
local active_bombers = {}

-- Ditto except for enemy fighters
local active_cap = {}

-- Ditto except for general ground attacking planes (i.e. not strategic bombers).  Not implemented.
local active_attackers = {}

local active_bomber_escorts = {}

local active_ace = nil

-- Kill these to get bombers!
local intel_targets = {}

-- Game state, designed to only hold data easily serializable into a json file.  Same concept as GAW/PGAW
game_state = {
    objectives = {
        [1] = {"Senaki-Kolkhi", 0},
        [2] = {"Kutaisi", 0},
        [3] = {"FARP Alpha", 0},
        [4] = {"FARP Bravo", 0},
        [5] = {"FARP Charlie", 0},
        [6] = {"FARP Delta", 0},
        [7] = {"Tbilisi-Lochini", 0},
    },
    counterattacks = {}, -- Keeps up with which objectives a counter attack has been launched from so we can limit the number per objective
    checkpoints = {}, -- I'm calling what are essentially the BAI units from (P)GAW checkpoints here because they are generally along the route the main attacking force runs
    bombers = 5,
}

-- Convenience function to get what each sides active objective is.  I don't think I need to split by coalition but I'm leaving that in in case we decide to get creative with it
local getActiveObjectiveIndex = function(game_state, coalition)
    for idx,obj in ipairs(game_state.objectives) do
        if Airbase.getByName(obj[1]):getCoalition() ~= 2 then return idx end
    end
    -- Game is over I guess
end

mist.scheduleFunction(function()
    for idx, ap_data in pairs(game_state.objectives) do
        game_state.objectives[idx] = {ap_data[1], Airbase.getByName(ap_data[1]):getCoalition() }
    end
end, {}, timer.getTime() + 30, 60)


-- The active assault unit for the blue coalition.  Used in the function immediately below to ensure that an attacking force is launched from the most forward objective
-- that blue owns.  Will probably get serialized in some fashion.
local active_assault_group = {}
mist.scheduleFunction(function() 
    local obj_idx = getActiveObjectiveIndex(game_state, 2)
    local target_assault_group = active_assault_group[obj_idx]
    if not HOGGIT.GroupIsAlive(target_assault_group) or target_assault_group:getSize() < 4 then
        active_assault_group[obj_idx] = HOGGIT.spawners.blue['blue_assault_' .. obj_idx]:Spawn()
        
        feba_ab = "Northeast Poti"
        if obj_idx > 1 then feba_ab = game_state.objectives[obj_idx - 1][1] end
        trigger.action.outText("NATO assault forces have been repelled.  Mustering forces for a new attack on " .. game_state.objectives[obj_idx][1] .. " from " .. feba_ab, 40)
    end
end, {}, timer.getTime() + 10, 600)

-- Same concept but for red.  Basically the same except it will bail out if a red force has already been sent from this objective (see: game_state.counterattacks).
local red_active_assault_group = nil
mist.scheduleFunction(function() 
    if not HOGGIT.GroupIsAlive(red_active_assault_group) then
        local obj_idx = getActiveObjectiveIndex(game_state, 1)
        if game_state.counterattacks[obj_idx] == nil then
            red_active_assault_group = HOGGIT.spawners.red['red_assault_' .. obj_idx]:Spawn()
            
            feba_ab = game_state.objectives[obj_idx][1]
            trigger.action.outText("Russian ground forces are departing " .. feba_ab .. " to intercept our assault forces!", 40)
            game_state.counterattacks[obj_idx] = true
        end
    end
end, {}, timer.getTime() + 10, 1200)


function allOnGround(group)
    local grp = nil
    local allOnGround = true
    if type(group) == "string" then
        grp = Group.getByName(group)
    else
        grp = group
    end
    if not HOGGIT.GroupIsAlive(grp) then return false end

    for i,unit in ipairs(grp:getUnits()) do
        if unit:inAir() then allOnGround = false end
    end

    return allOnGround
end


-- Conveience function to send a group toward an objective at the same altitide as the group they are escorting.  This is a simple hack done because the in game "escort" task
-- breaks if you push or set a task for the escorted unit, which is unfortunately exactly how I did bomber spawns.  This gets pretty close to the desired effect after a lot
-- of experimentation if you spawn them relatively close together.
escortTarget = function(group, point)
    return {
        ["id"] = "Mission",
        ["params"] = {
            ['route'] = {
                ['points'] = {
                    [1] = {
                        ['type'] = "Turning Point",
                        ['action'] = "Fly Over Point",
                        ['alt'] = group:getUnit(1):getPosition().p.y,
                        ['x'] = point.x,
                        ['y'] = point.z,
                        ["speed"] = 178.5
                    }
                }
            }
        }
    }
end

-- Sends a bomber to a point if the team has them available
local sendBomber = function(event)
    if event.id == world.event.S_EVENT_MARK_CHANGE then
        trigger.action.outText(event.text, 20)
        if event.text == "b52" or event.text == "B52" then
            if game_state.bombers > 0 then
                spawn_bomber_point_attack(event.pos, 2)
                game_state.bombers = game_state.bombers - 1
                if game_state.bombers < 0 then game_state.bombers = 0 end
            end
        end
    end
end

mist.addEventHandler(sendBomber)

-- Spawns a bomber and sends them after an arbitray point
spawn_bomber_point_attack= function(point, coalition)
    if coalition == 2 then
        local bomber = HOGGIT.spawners.blue["blue_bomber"]:Spawn()
        trigger.action.outText("STRATEGIC BOMBER LAUNCHING\n==========\nA B-52 bomber is orbiting Kobuleti, it will leave in about 5 minutes to attack the enemy front!  Make sure he arrives and delivers his weapons!", 60)
        active_bombers[bomber] = 9999

        local bombTarget = function(x, y)
            return {
                ["id"] = "Bombing",
                ["params"] = {
                    ["x"] = x,
                    ["y"] = y,
                    ["attackQty"] = 1,
                    ["expend"] = "All",
                    ["groupAttack"] = false,
                }
            }
        end

        local waitTime = 60
        if coalition == 2 then waitTime = 2 end
        mist.scheduleFunction(function()
            bomber:getController():setTask(bombTarget(point.x, point.z))
            bomber:getController():setOption(1,0)
        end, {}, timer.getTime() + waitTime, 60, timer.getTime() + 1200)
    end
end

-- Spawns a bomber and an escort and sends them on their merry way.  Point specified will eat approximately 100 FAB250s.
spawn_bomber_mission = function(target, coalition)
    -- probably need to extract this little snippet that builds a table of available spawns and 
    -- pushes back spawn zone 3 to spawnzone 4 if the players own FARP Bravo into it's own function
    local available_spawn_zones = {1, 2,}
    if Airbase.getByName(game_state.objectives[4][1]):getCoalition() ~= 2 then
        table.insert(available_spawn_zones, 3)
    else
        table.insert(available_spawn_zones, 4)
    end

    -- Get the final spawn zones, then adjust the escorts to spawn with an offset so they don't crowd each other
    local zone_idx = HOGGIT.randomInList(available_spawn_zones)
    local spawn_zone = "bomber_zone_" .. zone_idx
    local bomber_spawn_point = mist.utils.zoneToVec3(spawn_zone)

    bomber_spawn_point.y = 3870

    -- Function to build a bombing task at a given point.  Can pass directly to setTask/pushTask
    local bombTarget = function(positionable)
        return {
            ["id"] = "Bombing",
            ["params"] = {
                ["x"] = positionable:getPosition().p.x,
                ["y"] = positionable:getPosition().p.z,
                ["attackQty"] = 1,
                ["expend"] = "All",
                ["groupAttack"] = false,
            }
        }
    end
    
    -- Get the correct coalitions stuff spawned up and stuffed into these variables.
    local bomber = nil
    local bomber_escort = nil

    if coalition == 1 then
        local bombunit = "red_bomber"
        if math.random(1,100) < 5 then
            bombunit = "ACE_LAZYBOOT"
            trigger.action.outText("RADAR CONTACT\n===============\n\nAce pilot LAZYBOOT has been spotted near Beslan with a nuclear weapon and is heading this way!", 30)
            trigger.action.outSound("alarm.ogg")
        end
        bomber = HOGGIT.spawners.red[bombunit]:SpawnAtPoint(bomber_spawn_point)
        bomber_escort = HOGGIT.spawners.red["red_bomber_escort_" .. zone_idx]:Spawn()
    elseif coalition == 2 then
        bomber = HOGGIT.spawners.blue["blue_bomber"]:Spawn()
    end

    if bomber_escort then
        active_bomber_escorts[bomber_escort] = bomber_escort:getName()
    end

    -- setTasks need to be scheduled later to prevent a bug/crash.  I think you technically only need 1 second but I use these to accomdate the turn radius
    -- of the escorts (which is ironically slower) so that they arrive at the same point around the same time.
    local waitTime = 60
    if coalition == 2 then waitTime = 1 end
    mist.scheduleFunction(function()
        bomber:getController():setTask(bombTarget(target))
        bomber:getController():setOption(1,0)
    end, {}, timer.getTime() + waitTime, 60, timer.getTime() + 1200)

    -- Get a marker ID for the group and then place it where they spawned.
    local marker_id = getMarkerId()
    if coalition == 1 then
        trigger.action.outText("RADAR CONTACT\n==========\nBreak break!\nAn incoming russian strategic bomber has been detected!\n\nAssigned ID: ".. marker_id ..", Check the map for his position and his expected target!\n\n(This marker will be removed once the threat is no longer a factor)", 60)
        trigger.action.markToAll(marker_id, "BOMBER ".. marker_id .." DETECTED HERE", bomber:getUnit(1):getPosition().p, true)
        -- Get a second marker ID for the target location and place it.
        local target_id = getMarkerId()
        trigger.action.markToAll(target_id, "BOMBER ".. marker_id .." EXPECTED TARGET", target:getPosition().p, true)
        -- Add this bomber group to the dictionary of active bombers and store it's marker ID here.  This allows us to clean up the marker later.
    else
        trigger.action.outText("STRATEGIC BOMBER LAUNCHING\n==========\nA B-52 bomber is orbiting Kobuleti, it will leave in about 5 minutes to attack the enemy front!  Make sure he arrives and delivers his weapons!", 60)
    end

    active_bombers[bomber] = marker_id
end

-- Check every minute if each active bomber is still alive.  If they are no longer alive, remove them from the active_bombers dictionary and clean up their map markers.
mist.scheduleFunction(function()
    for group, marker_id in pairs(active_bombers) do
        if not HOGGIT.GroupIsAlive(group) then
            trigger.action.removeMark(marker_id)
            trigger.action.removeMark(marker_id + 1)
            active_bombers[group] = nil
        end
    end
end, {}, timer.getTime() + 60, 60)

local statefile = io.open(lfs.writedir() .. "Scripts\\GACW\\state.json", 'r')

if statefile then
        -- A statefile was found
        local state = statefile:read("*all")
        statefile:close()
        game_state = json:decode(state)
else
    -- Start a new situation.  Set the game state to all objectives set to red
    for idx,obj in ipairs(game_state.objectives) do
        game_state.objectives[idx][2] = 1
    end
end

-- Spawn some defense forces to properly set the airfields owner.  God I hate this hack so much but there's literally no other way.
for idx,obj in ipairs(game_state.objectives) do
    if obj[2] == 1 then
        local p = Airbase.getByName(obj[1]):getPosition().p
        p.x = p.x - 200
        p.z = p.z - 400
        HOGGIT.spawners.red["red_defense"]:SpawnAtPoint(p)
    elseif obj[2] == 2 then
        local p = Airbase.getByName(obj[1]):getPosition().p
        p.x = p.x - 200
        p.z = p.z - 400
        HOGGIT.spawners.blue["blue_defense"]:SpawnAtPoint(p)
    end
end

-- Spawn the checkpoints for the first area.  Additional areas not yet implemented, but to make them the first digit after "cp" will be it.  Co-locate cp_x_y zones in the ME
-- to get a nice natural progression of ground forces.
mist.scheduleFunction(function()
    if getActiveObjectiveIndex(game_state, 2) < 2 then
        for i = 1,2 do
            local point = mist.getRandomPointInZone('cp_1_' .. i)
            HOGGIT.spawners.red['red_checkpoint']:SpawnAtPoint(point)
        end
    end

    local used_sa_spawns = {}
    local start = 1
    if getActiveObjectiveIndex(game_state, 2) > 2 then start = 2 end
    for i = start, 2 do
        local spawn_candidate
        for x = 1, 3 do
            while true do
                spawn_candidate = 'SA_'.. i ..'_' .. math.random(1,13)
                if used_sa_spawns[spawn_candidate] == nil then
                    used_sa_spawns[spawn_candidate] = true
                    break
                end
            end
            local point = mist.getRandomPointInZone(spawn_candidate)
            HOGGIT.spawners.red['SA-2']:SpawnAtPoint(point)
            table.insert(used_sa_spawns, spawn_candidate)
        end

        while true do
            spawn_candidate = 'SA_'.. i ..'_' .. math.random(1,13)
            if used_sa_spawns[spawn_candidate] == nil then
                used_sa_spawns[spawn_candidate] = true
                break
            end

            local point = mist.getRandomPointInZone(spawn_candidate)
            HOGGIT.spawners.red['SA-6']:SpawnAtPoint(point)
            table.insert(used_sa_spawns, spawn_candidate)
        end
    end

    local used_cp_spawns = {}
    for i = getActiveObjectiveIndex(game_state, 2), 5 do
        local spawn_candidate
        for x = 1, 2 do
            while true do
                spawn_candidate = 'cp_'.. i ..'_' .. math.random(1,9)
                if used_cp_spawns[spawn_candidate] == nil then
                    used_cp_spawns[spawn_candidate] = true
                    break
                end
            end
            local point = mist.getRandomPointInZone(spawn_candidate)
            HOGGIT.spawners.red['red_checkpoint']:SpawnAtPoint(point)
            table.insert(used_cp_spawns, spawn_candidate)
        end
    end

    -- Spawn enemy EWR
    idx = math.random(1,3)
    HOGGIT.spawners.red['EWR_375']:SpawnInZone('EWR-' .. idx)

    idx = math.random(1,3)
    HOGGIT.spawners.red['EWR_165']:SpawnInZone('EWR-' .. idx)

    idx = math.random(4,6)
    HOGGIT.spawners.red['EWR_375']:SpawnInZone('EWR-' .. idx)

    idx = math.random(4,6)
    HOGGIT.spawners.red['EWR_165']:SpawnInZone('EWR-' .. idx)

    -- Spawn chopper objectives
    for i = 1,2 do
        local idx = math.random(1,3)
        local grp = HOGGIT.spawners.red['red_light_def']:SpawnInZone('light_tgt_' .. i .. '_' .. idx)
        intel_targets[grp] = false
        local marker_id = getMarkerId()
        trigger.action.markToAll(marker_id, "OUTPOST ".. marker_id, grp:getUnit(1):getPosition().p, true)
    end
end, {}, timer.getTime() + 30)

local write_state = function()
    log("Writing State...")
    local stateFile = lfs.writedir()..[[Scripts\GACW\state.json]]
    local fp = io.open(stateFile, 'w')
    fp:write(json:encode(game_state))
    fp:close()
    log("Done writing state.")
end

mist.scheduleFunction(write_state, {}, timer.getTime() + 60, 580)

-- Testing starts here
missionCommands.addCommand("Available Bomber Strikes", nil, function() trigger.action.outText("You have " .. game_state.bombers .. " bomber strikes available.", 10) end)

-- Check if any intel targets are dead.  Add a bomber if they are
mist.scheduleFunction(function()
    for grp, destroyed in pairs(intel_targets) do
        if not destroyed and not HOGGIT.GroupIsAlive(grp) then
            -- These guys were just destroyed.  Set their state and add a bomber
            trigger.action.outText("An outpost containing intel has been secured!  A B-52 strike is now available!\nPlace a marker on the F10 map and and set the text to 'B52' and a bomber will attack that spot!", 20)
            intel_targets[grp] = true
            game_state.bombers = game_state.bombers + 1
        end
    end
end, {}, timer.getTime() + 120, 120)

-- Spawn bombers
mist.scheduleFunction(function()
    local bomber_count = 0
    local blue_bomber_count = 0
    local obj_idx = getActiveObjectiveIndex(game_state, 2)
    for grp, marker_id in pairs(active_bombers) do 
        if allOnGround(grp) then
            grp:destroy()
            active_bombers[grp] = nil
        elseif HOGGIT.GroupIsAlive(grp) then 
            if grp:getCoalition() == 1 then
                bomber_count = bomber_count + 1
            else
                blue_bomber_count = blue_bomber_count + 1
            end
        else 
            active_bombers[grp] = nil
        end
    end

    if bomber_count == 0 then bomber_count = 1 end
    for i=bomber_count,1 do
        mist.scheduleFunction(function()
            if HOGGIT.GroupIsAlive(active_assault_group[obj_idx]) then
                spawn_bomber_mission(active_assault_group[obj_idx]:getUnit(1), 1)
            end
        end, {}, timer.getTime() + math.random(10, 60))
    end

    -- blue bombers
    if blue_bomber_count == 0 and HOGGIT.GroupIsAlive(red_active_assault_group) then
        mist.scheduleFunction(function()
            if HOGGIT.GroupIsAlive(red_active_assault_group) then
                spawn_bomber_mission(red_active_assault_group:getUnit(1), 2)
            end
        end, {}, timer.getTime() + math.random(180, 600))
    end

end, {}, timer.getTime() + 600, 1300)

-- Spawn Ships
local shipidx = math.random(1,3)
HOGGIT.spawners.red["ships"..shipidx]:Spawn()

-- Spawn Aces
-- mist.scheduleFunction(function()
--     if HOGGIT.GroupIsAlive(active_ace) then
--        return
--     end

--     if allOnGround(active_ace) then
--         active_ace:destroy()
--     end

--     local aces = {'LAWLCAT', 'ACIDIC', 'JERS', 'SQUINKYS'}
--     local ace = HOGGIT.randomInList(aces)
--     active_ace = HOGGIT.spawners.red['ACE_' .. ace]:Spawn()
--     trigger.action.outText("RADAR CONTACT\n===============\n\nAce pilot " .. ace .. " has been spotted near Beslan in an advanced fighter prototype and is heading this way!", 30)

-- end, {}, timer.getTime() + 1800, 2000)

-- Ensure there's 4-5 CAP units in the air at all times.  Randomize the group selected, and then inform the players
mist.scheduleFunction(function()
    -- hijacking this function to cleanup bomber escorts
    for grp, grp_name in pairs(active_bomber_escorts) do
        if allOnGround(grp) then
            grp:destroy()
            active_bomber_escorts[grp] = nil
        end
    end

    -- Used to make the player report outText
    local spawn_locations = {}

    -- Keep up with if we actually spawned anything
    local did_spawn = false

    -- Ge tthe count of alive units in our list here
    local cap_count = 0
    for idx, group in ipairs(active_cap) do
        if allOnGround(group) then
            group:destroy()
        elseif HOGGIT.GroupIsAlive(group) then
            cap_count = cap_count + 1
        end
    end

    -- That snippet that builds zones from earlier
    local spawn_zones = {'bomber_zone_1', 'bomber_zone_2'} 
    if Airbase.getByName(game_state.objectives[4][1]):getCoalition() ~= 2 then
        table.insert(spawn_zones, 'bomber_zone_3')
    else
        table.insert(spawn_zones, 'bomber_zone_4')
    end

    for i=cap_count,2 do
        did_spawn = true
        local spawn_area = math.random(1,2)
        local spawn_type = math.random(1,4)

        -- Poor man's set to build the player report at the end of this function
        if spawn_area == 1 then 
            spawn_locations['Sukhumi-Babashara'] = true 
        elseif spawn_area == 2 then 
            spawn_locations['Beslan'] = true 
        end

        local grp = HOGGIT.spawners.red['CAP-' .. spawn_type .. '-' .. spawn_area]:Spawn()

        -- Add the new group to the list.  Must delay to prevent bug/crash
        mist.scheduleFunction(function(grp)
            table.insert(active_cap, grp)
        end, {grp}, timer.getTime() + 2)
    end

    -- Report to players
    if did_spawn then
        local outString = "RADAR CONTACT:\n==================\nIncoming hostile fighters inbound from:\n"
        for key,_ in pairs(spawn_locations) do outString = outString .. key .. "\n"end
        trigger.action.outText(outString, 30)
    end
end, {}, timer.getTime() + 300, 1200)
